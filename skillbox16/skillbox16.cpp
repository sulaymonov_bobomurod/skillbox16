﻿#include <iostream>

using namespace std;

int main()
{
	const int size(6);
	int date;
	int myarray[size][size];
	for (int i = 0; i < size; i++)
	{
		int k = i;
		for (int j = 0; j < size; j++)
		{
			myarray[i][j] = k;
			k++;
			cout << myarray[i][j]<<" ";
		}
		cout << endl;
	}
	cout << "Enter date:"; cin >> date;
	int index = (int)date / size;
	int sum = 0;
	for (int j = 0; j < size; j++)
	{
		sum += myarray[index][j];
	}
	cout << "Sum of " << index << " - row: " << sum << endl;

}
